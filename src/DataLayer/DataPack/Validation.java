package DataLayer.DataPack;

import java.io.IOException;

public class Validation {

    public static float validateFloat(String value) throws IOException{

        try{

            return Float.parseFloat(value);
        }catch (Exception e){

            throw new IOException("Invalid format for price field");
        }
    }
}

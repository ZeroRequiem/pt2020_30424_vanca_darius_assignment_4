package DataLayer.DataPack;

import BussinesLayer.Restaurant.*;

import java.io.*;

public class RestaurantSerialization {

    public static void writeToFile(Restaurant restaurant){

        try {
            FileOutputStream fileOut = new FileOutputStream("restaurant.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(restaurant);
            out.close();
            fileOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Restaurant obtainObject(String location){

        Restaurant restaurant = null;

        try {
            FileInputStream fileIn = new FileInputStream(location);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            restaurant = (Restaurant) in.readObject();
            in.close();
            fileIn.close();
        } catch (Exception i) {
            System.out.println(i.getMessage());
            i.printStackTrace();
        }

        return restaurant;
    }
}

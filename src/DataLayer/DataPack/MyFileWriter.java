package DataLayer.DataPack;

import BussinesLayer.ObserverPack.MyObserver;
import BussinesLayer.Restaurant.Order;
import BussinesLayer.Restaurant.*;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MyFileWriter {

    public static void generateBill(Order order, float price, String outputFile){

        try {

            BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));

            writer.write("Bill\n");
            writer.write("OrderId:" + order.getOrderID() + "\n");
            writer.write("Date:" + order.getDate() + "\n");
            writer.write("Content of order:\n");

            for(MenuItem current : order.getTable()){

                writer.write("-" + current.getName() + "\n");
            }

            writer.write("Total to pay:" + price);
            writer.close();

        }catch (IOException e){

            System.out.println(e.getMessage());
            System.exit(-1);
        }


    }

    public static void saveToFile(Restaurant restaurantProcessing){

        List<MyObserver> save = new ArrayList<>(restaurantProcessing.getObservers());
        restaurantProcessing.clearObservers();
        RestaurantSerialization.writeToFile(restaurantProcessing);
        restaurantProcessing.setObservers(save);
    }
}

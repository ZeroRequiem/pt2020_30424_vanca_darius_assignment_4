package PresentationLayer.JComponents;

import javax.swing.*;
import java.awt.*;

public class CustomComboBox extends JComboBox <String>{

    public CustomComboBox(String[] content, int width, int height){

        super(content);

        this.setBackground(Color.darkGray);
        this.setForeground(Color.lightGray);
        this.setPreferredSize(new Dimension(width, height));
        this.setFont(new Font("Montserrat", Font.BOLD, 16));
    }

    public CustomComboBox(String[] content){

        this(content, 200, 26);
    }
}

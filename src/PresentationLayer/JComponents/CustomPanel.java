package PresentationLayer.JComponents;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class CustomPanel extends JPanel {

    public CustomPanel(int width, int height){

        super();

        this.setPreferredSize(new Dimension(width, height));
        this.setBackground(new Color(50,50,50));
        this.setBorder(new EmptyBorder(10,10,10,10));
    }

    public CustomPanel(){

        this(400, 60);
    }
}

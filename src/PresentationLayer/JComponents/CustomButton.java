package PresentationLayer.JComponents;

import javax.swing.*;
import java.awt.*;

public class CustomButton extends JButton {

    public CustomButton(String text, int width, int height){

        super(text);

        this.setBackground(Color.darkGray);
        this.setForeground(Color.lightGray);
        this.setSize(new Dimension(width, height));
        this.setFont(new Font("Montserrat", Font.BOLD, 14));

    }

    public CustomButton(String text){

        this(text, 50, 20);
    }
}

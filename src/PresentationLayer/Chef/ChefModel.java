package PresentationLayer.Chef;

import BussinesLayer.Restaurant.Restaurant;

public class ChefModel {

    private Restaurant restaurantProcessing;

    public ChefModel(){

        restaurantProcessing = new Restaurant();
    }

    public void setRestaurantProcessing(Restaurant restaurantProcessing) {
        this.restaurantProcessing = restaurantProcessing;
    }

    public Restaurant getRestaurantProcessing() {
        return restaurantProcessing;
    }


    public String[][] getMenuDetails(){
        return restaurantProcessing.getMenuDetails();
    }

    public String[][] getOrdersDetails(){
        return restaurantProcessing.getOrdersDetails();
    }
}

package PresentationLayer.Chef;

import BussinesLayer.EnumPack.Panel;
import PresentationLayer.JComponents.CustomButton;
import PresentationLayer.JComponents.CustomPanel;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.event.ActionListener;

public class ChefView extends JFrame {

    private BussinesLayer.EnumPack.Panel currentPanel = BussinesLayer.EnumPack.Panel.CHEF_MENU;

    private JPanel mainPanel = new JPanel();
    private JPanel displayMenuPanelMain = new JPanel();
    private JPanel displayOrdersPanelMain = new JPanel();
    private JLabel chefLabel = new JLabel("PresentationLayer.Chef Window");

    private JPanel menuItemsPanel = new CustomPanel(400, 310);
    private JTable menuTable = new JTable(new String[][]{{"Empty", "Empty", "Empty", "Empty"}}, new String[]{"Name", "Price", "Composition"});
    private JTable orderTable = new JTable(new String[][]{{"Empty", "Empty", "Empty", "Empty", "Empty"}}, new String[]{"OrderID", "Date", "Price", "Composition"});
    private JPanel ordersPanel = new CustomPanel(400, 300);

    private JScrollPane tableScroll;
    private JScrollPane orderScroll;
    
    private JButton displayMenuButton = new CustomButton("Display Menu");
    private JButton displayOrdersButton = new CustomButton("Display Orders");
    
    private JPanel selectPanel = new JPanel();

    private void generateSelectPanel() {

        selectPanel.add(displayMenuButton);
        selectPanel.add(Box.createRigidArea(new Dimension(20, 0)));
        selectPanel.add(displayOrdersButton);

        selectPanel.setPreferredSize(new Dimension(400, 60));
        selectPanel.setBackground(new Color(50, 50, 50));
        selectPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
    }

    private void setOrderScroll(){

        orderScroll = new JScrollPane(orderTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        orderTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        orderScroll.setPreferredSize(new Dimension(400, 300));
        ordersPanel.setBorder(new EmptyBorder(0, 0, 0, 0));
        TableColumnModel tableColumnModel = orderTable.getColumnModel();
        tableColumnModel.getColumn(3).setPreferredWidth((int) (0.5 * 400));
        tableColumnModel.getColumn(2).setPreferredWidth((int) (0.1 * 400));
        tableColumnModel.getColumn(1).setPreferredWidth((int) (0.25 * 400));
        tableColumnModel.getColumn(0).setPreferredWidth((int) (0.15 * 400));
        tableColumnModel.setColumnSelectionAllowed(false);
        orderScroll.getViewport().setBackground(new Color(50, 50, 50));
    }

    private void setMenuItemsScroll(){

        tableScroll = new JScrollPane(menuTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        menuTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        tableScroll.setPreferredSize(new Dimension(400, 300));
        menuItemsPanel.setBorder(new EmptyBorder(0, 0, 0, 0));
        TableColumnModel tableColumnModel = menuTable.getColumnModel();
        tableColumnModel.getColumn(2).setPreferredWidth((int) (0.6 * 400));
        tableColumnModel.getColumn(1).setPreferredWidth((int) (0.15 * 400));
        tableColumnModel.getColumn(0).setPreferredWidth((int) (0.25 * 400));
        tableColumnModel.setColumnSelectionAllowed(false);
        tableScroll.getViewport().setBackground(new Color(50, 50, 50));
    }

    private void setEverything(){
        
        generateSelectPanel();
        
        displayMenuPanelMain.setBounds(0, 0, 600, 450);
        displayMenuPanelMain.setBackground(Color.darkGray);
        displayMenuPanelMain.setBorder(new EmptyBorder(25, 10, 10, 10));

        displayOrdersPanelMain.setBounds(0, 0, 600, 450);
        displayOrdersPanelMain.setBackground(Color.darkGray);
        displayOrdersPanelMain.setBorder(new EmptyBorder(25, 10, 10, 10));
    }
    
    private void generateMenuItemsPanel(String[][] data){
        
        displayMenuPanelMain.removeAll();
        displayMenuPanelMain.revalidate();
        displayMenuPanelMain.repaint();

        displayMenuPanelMain.add(selectPanel);

        if(data != null)
            menuTable = new JTable(data, new String[]{"Name", "Price", "Composition"});
        else
            menuTable = new JTable(new String[][]{{"Empty...", "Empty...", "Empty..."}}, new String[]{"Name", "Price", "Composition"});

        setMenuItemsScroll();

        menuItemsPanel.removeAll();
        menuItemsPanel.revalidate();
        menuItemsPanel.repaint();

        menuItemsPanel.add(tableScroll);

        displayMenuPanelMain.add(menuItemsPanel);
    }

    private void generateOrdersPanel(String[][] data){

        displayOrdersPanelMain.removeAll();
        displayOrdersPanelMain.revalidate();
        displayOrdersPanelMain.repaint();

        displayOrdersPanelMain.add(selectPanel);

        if(data != null)
            orderTable = new JTable(data, new String[]{"OrderID", "Date", "Price", "Composition"});
        else
            orderTable = new JTable(new String[][]{{"Empty", "Empty", "Empty", "Empty", "Empty"}}, new String[]{"OrderID", "Date", "Price", "Composition"});

        setOrderScroll();

        ordersPanel.removeAll();
        ordersPanel.revalidate();
        ordersPanel.repaint();

        ordersPanel.add(orderScroll);

        displayOrdersPanelMain.add(ordersPanel);
    }

    public ChefView(){

        this.setPreferredSize(new Dimension(600, 500));
        this.setBackground(Color.DARK_GRAY);
        this.setResizable(false);
        this.setTitle("ChefWindow");
        mainPanel.setBackground(Color.DARK_GRAY);
        mainPanel.setForeground(Color.DARK_GRAY);
        mainPanel.setPreferredSize(new Dimension(600, 350));


        setEverything();
        generateMenuItemsPanel(null);

        mainPanel.setBorder(new EmptyBorder(-10, 10, 10, 10));
        mainPanel.setLayout(new GridLayout(0, 1, 0, 0));
        mainPanel.add(displayMenuPanelMain);

        this.setContentPane(mainPanel);
        this.setVisible(true);
        this.pack();

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public void switchToOrders(String[][] data){

        currentPanel = BussinesLayer.EnumPack.Panel.CHEF_ORDERS;
        mainPanel.removeAll();
        mainPanel.revalidate();
        mainPanel.repaint();
        generateOrdersPanel(data);
        mainPanel.add(displayOrdersPanelMain);
    }

    public void switchToMenu(String[][] data){

        currentPanel = Panel.CHEF_MENU;
        mainPanel.removeAll();
        mainPanel.revalidate();
        mainPanel.repaint();
        generateMenuItemsPanel(data);
        mainPanel.add(displayMenuPanelMain);
    }

    public void refreshMenuItems(String[][] data){

        if(data != null)
            menuTable = new JTable(data, new String[]{"Name", "Price", "Composition"});
        else
            menuTable = new JTable(new String[][]{{"Empty...", "Empty...", "Empty..."}}, new String[]{"Name", "Price", "Composition"});

        setMenuItemsScroll();

        menuItemsPanel.removeAll();
        menuItemsPanel.revalidate();
        menuItemsPanel.repaint();

        menuItemsPanel.add(tableScroll);
    }

    public void refreshOrders(String[][] data){

        if(data != null)
            orderTable = new JTable(data, new String[]{"OrderID", "Date", "Price", "Composition"});
        else
            orderTable = new JTable(new String[][]{{"Empty", "Empty", "Empty", "Empty", "Empty"}}, new String[]{"OrderID", "Date", "Price", "Composition"});

        setOrderScroll();

        ordersPanel.removeAll();
        ordersPanel.revalidate();
        ordersPanel.repaint();

        ordersPanel.add(orderScroll);
    }

    public void addMenuItemsListener(ActionListener cbl){
        displayMenuButton.addActionListener(cbl);
    }

    public void addOrdersListener(ActionListener cbl){
        displayOrdersButton.addActionListener(cbl);
    }

    public BussinesLayer.EnumPack.Panel getCurrentPanel(){

        return currentPanel;
    }

    public void showMessage(String msg){

        JOptionPane.showMessageDialog(this, msg);
    }
}

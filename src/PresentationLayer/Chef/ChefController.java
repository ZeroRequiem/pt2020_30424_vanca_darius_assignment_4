package PresentationLayer.Chef;

import BussinesLayer.ObserverPack.*;
import BussinesLayer.EnumPack.Panel;
import BussinesLayer.EnumPack.Type;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ChefController extends MyObserver {

    private ChefModel model;
    private ChefView view;

    public ChefController(ChefModel model, ChefView view){

        this.view = view;
        this.model = model;

        model.getRestaurantProcessing().attachObserver(this);

        view.addMenuItemsListener(new MenuItemsListener());
        view.addOrdersListener(new OrdersListener());

        view.refreshOrders(model.getOrdersDetails());
        view.refreshMenuItems(model.getMenuDetails());

    }

    class MenuItemsListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            if(view.getCurrentPanel() != Panel.CHEF_MENU){

                view.switchToMenu(model.getMenuDetails());
            }
        }
    }

    class OrdersListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            if(view.getCurrentPanel() != Panel.CHEF_ORDERS){

                view.switchToOrders(model.getOrdersDetails());
            }
        }
    }

    @Override
    public void update(String msg, Type type) {

        if(type == Type.CHEF_MSG){

            view.showMessage(msg);
        }

        view.refreshMenuItems(model.getMenuDetails());
        view.refreshOrders(model.getOrdersDetails());
    }
}

package PresentationLayer.Waiter;

import BussinesLayer.ObserverPack.*;
import BussinesLayer.EnumPack.Panel;
import BussinesLayer.EnumPack.Type;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class WaiterController extends MyObserver {

    private WaiterView view;
    private WaiterModel model;

    public WaiterController(WaiterModel model, WaiterView view){

        this.model = model;
        this.view = view;

        this.model.attachObserver(this);

        view.addCreateButtonListener(new CreateButtonListener());
        view.addGenerateBillButtonListener(new GenerateBillButtonListener());
        view.addGenerateButtonListener(new GenerateButtonListener());
        view.addItemButtonListener(new AddItemsButtonListener());
        view.addPlaceOrderButtonListener(new PlaceOrderButtonListener());
        view.addRemoveItemFromOrderListener(new RemoveItemFromOrderListener());

        view.refreshMenuItems(model.getDetailsMenu());
        view.refreshOrders(model.getDetailsOrders());

    }

    class CreateButtonListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            if(view.getCurrentPanel() != Panel.CREATE){

                view.resetPanels();
                view.switchToCreate(model.getDetailsMenu());
            }
        }
    }

    class GenerateBillButtonListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            if(view.getCurrentPanel() != Panel.BILL){

                if(model.isFinishedPlacingOrder()) {

                    view.resetPanels();
                    model.emptyList();
                    view.switchToGenerateBill(model.getDetailsOrders());

                }else{

                    view.showMessage("Order is not finished placing");
                }
            }
        }
    }

    class AddItemsButtonListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            if(view.getSelectedItem() == null)
                return;

            if(model.isMenuEmpty()){

                view.showMessage("Menu is empty, please try again later");
                return;
            }

            model.addItem(view.getSelectedItem());
            view.refreshItemsInOrder(model.getItemsOfOrder());
        }
    }

    class PlaceOrderButtonListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            if(model.getItemsOfOrder().isEmpty()){

                view.showMessage("Order has no items");
                return;
            }

            model.placeOrder();
            view.refreshItemsInOrder(null);
            view.showMessage("Successfully placed order");
            model.getRestaurantProcessing().notifyAllObservers("A new order has been placed", Type.CHEF_MSG);
        }
    }

    class GenerateButtonListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            if(view.getSelectedOrder() == null)
                return;

            if(model.isOrderListEmpty()){

                view.showMessage("No orders were placed, try again later");
                return;
            }

            model.generateBill(Integer.parseInt(view.getSelectedOrder()));
            view.showMessage("Successfully generated bill");
        }
    }

    class RemoveItemFromOrderListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            if(view.getSelectedItemFromOrder() == null)
                return;

            if(model.isListOfItemsInOrderEmpty()){

                view.showMessage("Unable to perform remove, list of items in order is empty");
                return;
            }

            model.removeItemFromOrder(view.getSelectedItemFromOrder());
            view.refreshItemsInOrder(model.getItemsOfOrder());
        }
    }

    @Override
    public void update(String msg, Type type) {

        view.refreshMenuItems(model.getDetailsMenu());
        view.refreshOrders(model.getDetailsOrders());
    }
}

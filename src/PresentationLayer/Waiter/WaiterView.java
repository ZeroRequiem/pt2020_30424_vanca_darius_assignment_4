package PresentationLayer.Waiter;

import BussinesLayer.EnumPack.Panel;
import PresentationLayer.JComponents.CustomButton;
import PresentationLayer.JComponents.CustomPanel;

import javax.swing.JPanel;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.List;

public class WaiterView extends JFrame {

    private BussinesLayer.EnumPack.Panel currentPanel;

    private JPanel mainPanel = new JPanel();
    private JPanel createPanel = new CustomPanel();
    private JPanel createPanelMain = new JPanel();

    private JPanel menuItemsPanel = new CustomPanel(400, 210);

    private JPanel generateBillMainPanel = new JPanel();
    private JPanel generateBillPanel = new CustomPanel();

    private JPanel selectPanel = new JPanel();
    private JButton createButton = new CustomButton("Create");
    private JButton generateBillButton = new CustomButton("GenerateBill");
    private JButton placeOrderButton = new CustomButton("Place Order");

    private JTable menuTable = new JTable(new String[][]{{"Empty", "Empty", "Empty", "Empty"}}, new String[]{"Name", "Price", "Composition"});
    private JTable itemsInOrderTable;
    private JTable orderTable = new JTable(new String[][]{{"Empty", "Empty", "Empty", "Empty", "Empty"}}, new String[]{"OrderID", "Date", "Price", "Composition"});
    private JPanel ordersPanel = new CustomPanel(400, 300);
    private JPanel displayPanel = new CustomPanel(400, 300);
    private JScrollPane tableScroll;
    private JScrollPane orderScroll;
    private JScrollPane orderItemsScroll;
    private JPanel itemsInOrderPanel = new CustomPanel(400, 200);

    private JButton removeFromOrderButton = new CustomButton("Remove");
    private JButton addBaseToCompositeButton = new CustomButton("Add");
    private JButton addItemButton = new CustomButton("Add Items");
    private JButton generateButton = new CustomButton("Generate");

    private void generateSelectPanel() {

        selectPanel.add(createButton);
        selectPanel.add(Box.createRigidArea(new Dimension(20, 0)));
        selectPanel.add(generateBillButton);

        selectPanel.setPreferredSize(new Dimension(400, 60));
        selectPanel.setBackground(new Color(50, 50, 50));
        selectPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
    }

    private void setGenerateBillPanel() {

        generateBillMainPanel.setBounds(0, 0, 600, 350);
        generateBillMainPanel.setBackground(Color.darkGray);
        generateBillMainPanel.setBorder(new EmptyBorder(25, 10, 10, 10));
    }

    private void setOrderScroll(){

        orderScroll = new JScrollPane(orderTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        orderTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        orderScroll.setPreferredSize(new Dimension(400, 295));
        ordersPanel.setBorder(new EmptyBorder(0, 0, 0, 0));
        TableColumnModel tableColumnModel = orderTable.getColumnModel();
        tableColumnModel.getColumn(3).setPreferredWidth((int) (0.5 * 400));
        tableColumnModel.getColumn(2).setPreferredWidth((int) (0.1 * 400));
        tableColumnModel.getColumn(1).setPreferredWidth((int) (0.25 * 400));
        tableColumnModel.getColumn(0).setPreferredWidth((int) (0.15 * 400));
        tableColumnModel.setColumnSelectionAllowed(false);
        orderScroll.getViewport().setBackground(new Color(50, 50, 50));
    }

    private void generateBillPanel(String[][] data) {

        generateBillMainPanel.removeAll();
        generateBillMainPanel.revalidate();
        generateBillMainPanel.repaint();

        generateBillMainPanel.add(selectPanel);

        generateBillPanel.removeAll();
        generateBillPanel.revalidate();
        generateBillPanel.repaint();

        generateBillPanel.add(generateButton);
        generateBillMainPanel.add(generateBillPanel);

        if(data != null)
            orderTable = new JTable(data, new String[]{"OrderID", "Date", "Price", "Composition"});
        else
            orderTable = new JTable(new String[][]{{"Empty", "Empty", "Empty", "Empty", "Empty"}}, new String[]{"OrderID", "Date", "Price", "Composition"});

        setOrderScroll();

        ordersPanel.removeAll();
        ordersPanel.revalidate();
        ordersPanel.repaint();

        ordersPanel.add(orderScroll);

        generateBillMainPanel.add(ordersPanel);
    }

    private void setMenuItemsScroll(){

        tableScroll = new JScrollPane(menuTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        menuTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        tableScroll.setPreferredSize(new Dimension(400, 200));
        menuItemsPanel.setBorder(new EmptyBorder(0, 0, 0, 0));
        TableColumnModel tableColumnModel = menuTable.getColumnModel();
        tableColumnModel.getColumn(2).setPreferredWidth((int) (0.6 * 400));
        tableColumnModel.getColumn(1).setPreferredWidth((int) (0.15 * 400));
        tableColumnModel.getColumn(0).setPreferredWidth((int) (0.25 * 400));
        tableColumnModel.setColumnSelectionAllowed(false);
        tableScroll.getViewport().setBackground(new Color(50, 50, 50));
    }

    private void setItemsInOrderScroll(){

        orderItemsScroll = new JScrollPane(itemsInOrderTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        itemsInOrderTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        orderItemsScroll.setPreferredSize(new Dimension(400, 185));
        itemsInOrderPanel.setBorder(new EmptyBorder(0, 0, 0, 0));
        TableColumnModel tableColumnModel = itemsInOrderTable.getColumnModel();
        tableColumnModel.getColumn(2).setPreferredWidth((int) (0.6 * 400));
        tableColumnModel.getColumn(1).setPreferredWidth((int) (0.15 * 400));
        tableColumnModel.getColumn(0).setPreferredWidth((int) (0.25 * 400));
        tableColumnModel.setColumnSelectionAllowed(false);
        orderItemsScroll.getViewport().setBackground(new Color(50, 50, 50));
    }

    private void generateCreatePanel(String[][] data) {

        createPanelMain.removeAll();
        createPanelMain.revalidate();
        createPanelMain.repaint();

        createPanelMain.add(selectPanel);
        createPanelMain.add(createPanel);

        if(data != null)
            menuTable = new JTable(data, new String[]{"Name", "Price", "Composition"});
        else
            menuTable = new JTable(new String[][]{{"Empty...", "Empty...", "Empty..."}}, new String[]{"Name", "Price", "Composition"});

        setMenuItemsScroll();
        itemsInOrderTable = new JTable(new String[][]{{"Empty", "Empty", "Empty", "Empty"}}, new String[]{"Name", "Price", "Composition"});
        setItemsInOrderScroll();

        menuItemsPanel.removeAll();
        menuItemsPanel.revalidate();
        menuItemsPanel.repaint();

        menuItemsPanel.add(tableScroll);
        itemsInOrderPanel.add(orderItemsScroll);

        createPanelMain.add(createPanel);
        createPanelMain.add(menuItemsPanel);
        createPanelMain.add(itemsInOrderPanel);
    }

    private void setCreatePanel() {

        createPanelMain.setBounds(0, 0, 600, 350);
        createPanelMain.setBackground(Color.darkGray);
        createPanelMain.setBorder(new EmptyBorder(25, 10, 10, 10));

        createPanel.add(addItemButton);
        createPanel.add(Box.createRigidArea(new Dimension(10, 0)));
        createPanel.add(removeFromOrderButton);
        createPanel.add(Box.createRigidArea(new Dimension(10, 0)));
        createPanel.add(placeOrderButton);

    }

    private void setEverything() {

        currentPanel = BussinesLayer.EnumPack.Panel.CREATE;
        generateSelectPanel();
        setCreatePanel();
        setGenerateBillPanel();
    }

    public WaiterView() {

        this.setPreferredSize(new Dimension(600, 650));
        this.setBackground(Color.DARK_GRAY);
        this.setResizable(false);
        this.setTitle("Waiter Window");
        mainPanel.setBackground(Color.DARK_GRAY);
        mainPanel.setForeground(Color.DARK_GRAY);
        mainPanel.setPreferredSize(new Dimension(600, 350));


        setEverything();
        generateCreatePanel(null);

        mainPanel.setBorder(new EmptyBorder(-10, 10, 10, 10));
        mainPanel.setLayout(new GridLayout(0, 1, 0, 0));
        mainPanel.add(createPanelMain);

        this.setContentPane(mainPanel);
        this.setVisible(true);
        this.pack();

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

    public void switchToCreate(String[][] data) {

        currentPanel = BussinesLayer.EnumPack.Panel.CREATE;
        this.setResizable(true);
        this.setPreferredSize(new Dimension(600, 650));
        this.setResizable(false);
        this.pack();
        mainPanel.removeAll();
        mainPanel.revalidate();
        mainPanel.repaint();
        generateCreatePanel(data);
        mainPanel.add(createPanelMain);

    }

    public void switchToGenerateBill(String[][] data) {

        currentPanel = BussinesLayer.EnumPack.Panel.BILL;
        this.setResizable(true);
        this.setPreferredSize(new Dimension(600, 500));
        this.pack();
        this.setResizable(false);

        mainPanel.removeAll();
        mainPanel.revalidate();
        mainPanel.repaint();
        generateBillPanel(data);
        mainPanel.add(generateBillMainPanel);
    }

    public void resetPanels() {

        if (currentPanel == BussinesLayer.EnumPack.Panel.CREATE)
            resetCreatePanel();
    }

    private void resetCreatePanel() {

        refreshItemsInOrder(null);
    }

    public void addGenerateBillButtonListener(ActionListener cbl) {

        generateBillButton.addActionListener(cbl);
    }

    public void addCreateButtonListener(ActionListener cbl) {

        createButton.addActionListener(cbl);
    }

    public void addGenerateButtonListener(ActionListener cbl){

        generateButton.addActionListener(cbl);
    }

    public void addItemButtonListener(ActionListener cbl){

        addItemButton.addActionListener(cbl);
    }

    public void addPlaceOrderButtonListener(ActionListener cbl){

        placeOrderButton.addActionListener(cbl);
    }

    public void addRemoveItemFromOrderListener(ActionListener cbl){

        removeFromOrderButton.addActionListener(cbl);
    }

    public void setCurrentPanel(Panel panel) {

        currentPanel = panel;
    }

    public BussinesLayer.EnumPack.Panel getCurrentPanel() {

        return currentPanel;
    }

    public void showMessage(String msg) {

        JOptionPane.showMessageDialog(this, msg);
    }

    public void refreshMenuItems(String[][] data){

        if(data != null)
            menuTable = new JTable(data, new String[]{"Name", "Price", "Composition"});
        else
            menuTable = new JTable(new String[][]{{"Empty...", "Empty...", "Empty..."}}, new String[]{"Name", "Price", "Composition"});

        setMenuItemsScroll();

        menuItemsPanel.removeAll();
        menuItemsPanel.revalidate();
        menuItemsPanel.repaint();

        menuItemsPanel.add(tableScroll);
    }

    private void generateData(List<String> data){

        String[][] content = new String[data.size()][3];

        int itemsRowCount = 0;

        for (String current : data) {

            for (int row = 0; row < menuTable.getModel().getRowCount(); row++) {

                if (menuTable.getModel().getValueAt(row, 0).equals(current)) {

                    for (int col = 0; col < menuTable.getModel().getColumnCount(); col++)
                        content[itemsRowCount][col] = menuTable.getModel().getValueAt(row, col).toString();

                    itemsRowCount++;
                }
            }
        }

        itemsInOrderTable = new JTable(content, new String[]{"Name", "Price", "Composition"});
    }

    public void refreshItemsInOrder(List<String> data){

        if(data == null)
            itemsInOrderTable = new JTable(new String[][]{{"Empty...", "Empty...", "Empty..."}}, new String[]{"Name", "Price", "Composition"});
        else if(data.size() == 0)
            itemsInOrderTable = new JTable(new String[][]{{"Empty...", "Empty...", "Empty..."}}, new String[]{"Name", "Price", "Composition"});
        else
            generateData(data);

        setItemsInOrderScroll();

        itemsInOrderPanel.removeAll();
        itemsInOrderPanel.revalidate();
        itemsInOrderPanel.repaint();

        itemsInOrderPanel.add(orderItemsScroll);
    }

    public void refreshOrders(String[][] data){

        if(data != null)
            orderTable = new JTable(data, new String[]{"OrderID", "Date", "Price", "Composition"});
        else
            orderTable = new JTable(new String[][]{{"Empty", "Empty", "Empty", "Empty", "Empty"}}, new String[]{"OrderID", "Date", "Price", "Composition"});

        setOrderScroll();

        ordersPanel.removeAll();
        ordersPanel.revalidate();
        ordersPanel.repaint();

        ordersPanel.add(orderScroll);
    }

    public void refreshGenerateBillPanel(String[][] content) {

        if (content != null) {

            orderTable = new JTable(content, new String[]{"OrderID", "Date", "Price", "Composition"});
        }
    }

    public String getSelectedItemFromOrder(){

        if(itemsInOrderTable.getSelectedRow() == -1){

            showMessage("No items from order selected");
            return null;
        }

        return itemsInOrderTable.getModel().getValueAt(itemsInOrderTable.getSelectedRow(), 0).toString();
    }

    public String getSelectedItem(){

        if(menuTable.getSelectedRow() == -1){

            showMessage("No item selected to add to order");
            return null;
        }

        return menuTable.getModel().getValueAt(menuTable.getSelectedRow(), 0).toString();
    }

    public String getSelectedOrder(){

        if(orderTable.getSelectedRow() == -1){

            showMessage("No order selected");
            return null;
        }

        return orderTable.getModel().getValueAt(orderTable.getSelectedRow(), 0).toString();
    }
}


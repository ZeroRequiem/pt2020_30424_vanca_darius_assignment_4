package PresentationLayer.Waiter;

import BussinesLayer.Restaurant.*;
import BussinesLayer.ObserverPack.*;

import java.util.ArrayList;
import java.util.List;

public class WaiterModel {

    private List<String> itemsOfOrder;
    private Restaurant restaurantProcessing;
    private boolean finishedPlacingOrder = true;

    public Restaurant getRestaurantProcessing() {
        return restaurantProcessing;
    }

    public WaiterModel(){

        itemsOfOrder = new ArrayList<String>();
        restaurantProcessing = new Restaurant();
    }

    public void setRestaurantProcessing(Restaurant restaurantProcessing) {
        this.restaurantProcessing = restaurantProcessing;
    }

    public void addItem(String item){

        finishedPlacingOrder = false;
        itemsOfOrder.add(item);
    }

    public List<String> getItemsOfOrder(){
        return itemsOfOrder;
    }

    public void emptyList(){

        finishedPlacingOrder = true;
        itemsOfOrder.clear();
    }

    public void generateBill(int orderId){

        restaurantProcessing.generateBill(orderId);
    }

    public void placeOrder(){

        restaurantProcessing.createNewOrder(itemsOfOrder);
        restaurantProcessing.saveData();
        emptyList();
    }

    public String[][] getDetailsMenu(){

        return restaurantProcessing.getMenuDetails();
    }

    public String[][] getDetailsOrders(){

        return restaurantProcessing.getOrdersDetails();
    }

    public void attachObserver(MyObserver observer){

        restaurantProcessing.attachObserver(observer);
    }

    public void removeItemFromOrder(String item){

        itemsOfOrder.remove(item);

        if(itemsOfOrder.isEmpty())
            finishedPlacingOrder = true;
    }

    public boolean isMenuEmpty(){

        return restaurantProcessing.getMenu().isEmpty();
    }

    public boolean isListOfItemsInOrderEmpty(){

        return itemsOfOrder.isEmpty();
    }

    public boolean isOrderListEmpty(){

        return restaurantProcessing.getMap().isEmpty();
    }

    public boolean isFinishedPlacingOrder(){
        return finishedPlacingOrder;
    }
}

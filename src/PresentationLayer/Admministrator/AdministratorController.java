package PresentationLayer.Admministrator;

import BussinesLayer.EnumPack.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.Serializable;

public class AdministratorController implements Serializable {

    private AdministratorModel model;
    private AdministratorView view;

    public AdministratorController(AdministratorModel model, AdministratorView view){

        this.model = model;
        this.view = view;

        view.addDeleteButtonListener(new DeleteButtonListener());
        view.addCreateButtonListener(new CreateButtonListener());
        view.addEditButtonListener(new EditButtonListener());
        view.addDiplayButtonListener(new DisplayButtonListener());

        view.addOptionsListener(new OptionListener());
        view.addCreateItemButtonListener(new AddItemListener());
        view.addBaseToCompositeButtonListener(new AddToCompositeListener());

        view.addFinishDeleteListener(new DeleteItemListener());

        view.addNameOptionListener(new NameOptionListener());
        view.addPriceOptionListener(new PriceOptionListener());
        view.addCompostionOptionListener(new CompositeOptionListener());
        view.addFinishEditButtonListener(new FinishEditListener());
        view.addDeleteFromCompositeListener(new DeleteFromCompositeListener());
        view.addSelectFromMenuToEditListener(new SelectToEditListener());
    }

    class CreateButtonListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            if(view.getCurrentPanel() != Panel.CREATE) {

                view.resetPanels();
                view.switchToCreate();
                model.setCurrentItem(null);
                model.setCurrentTypeSelected(Type.BASE);
            }
        }
    }

    class EditButtonListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            if(view.getCurrentPanel() == Panel.CREATE && !model.isFinishedAdding()){

                view.showMessage("Composite item is empty");
                return;
            }

            if(view.getCurrentPanel() != Panel.EDIT) {

                view.resetPanels();
                view.switchToEdit();

                view.refreshMenuEdit(model.getContentMenu(), null, null);
                model.setCurrentTypeSelected(Type.BASE);
                view.addSelectFromMenuToEditListener(new SelectToEditListener());
            }
        }
    }

    class DeleteButtonListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            if(view.getCurrentPanel() == Panel.CREATE && !model.isFinishedAdding()){

                view.showMessage("Composite item is empty");
                return;
            }

            if(view.getCurrentPanel() != Panel.DELETE) {

                view.resetPanels();
                view.switchToDelete();
                model.setCurrentItem(null);
                model.setCurrentTypeSelected(Type.BASE);
                view.refreshDeletePanel(model.getContentMenu());
            }
        }
    }

    class DisplayButtonListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            if(view.getCurrentPanel() == Panel.CREATE && !model.isFinishedAdding()){

                view.showMessage("Composite item is empty");
                return;
            }

            model.setCurrentItem(null);

            view.resetPanels();

            if(model.getContentMenu().length == 0)
                view.refreshDisplayPanel(new String[][]{{"Empty...", "Empty...", "Empty..."}});
            else
                view.refreshDisplayPanel(model.getMenuDetails());

            view.switchToDisplay(0);
        }
    }

    class OptionListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            Type aux;

            if(!model.isFinishedAdding() && model.getCurrentTypeSelected() == Type.COMPOSITE){

                view.setIndexOption();
                view.showMessage("Composite item is empty");
                return;
            }

            if (view.getOption().equals("Base"))
                aux = Type.BASE;
            else
                aux = Type.COMPOSITE;


            if(model.getCurrentTypeSelected() != aux) {

                model.setCurrentTypeSelected(aux);
                view.setName("");
                view.setPrice("");
                view.setCreateCompositeVisible(false);

                if (aux == Type.BASE) {

                    view.setPriceVisible(true);

                }else {
                    view.setPriceVisible(false);
                }

            }
        }
    }

    class AddItemListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            if(view.getName().equals("")) {

                view.showMessage("Name field is empty");
                view.setName("");
                return;
            }

            if(view.getPrice().equals("") && model.getCurrentTypeSelected() == Type.BASE){

                view.showMessage("Price field is empty");
                view.setPrice("");
                return;
            }

            if(view.getCurrentPanel() == Panel.CREATE && !model.isFinishedAdding()){

                view.showMessage("Composite item is empty");
                view.setName("");
                return;
            }

            model.setCurrentItem(view.getName());
            view.setName("");

            try {

                if(model.getCurrentTypeSelected() == Type.BASE)
                    model.addItem(view.getPrice());
                else
                    model.addItem("0");

            }catch (IOException exc){

                view.showMessage(exc.getMessage());
                view.setPrice("");
                return;
            }

            if(model.getCurrentTypeSelected() == Type.COMPOSITE) {

                model.setFinishedAdding(false);
                view.setCreateCompositeVisible(true);
                model.restaurantProcessing.notifyAllObservers("A new composite item has been added to the menu", Type.CHEF_MSG);
            }

            view.refreshMenuCreate(model.getContentMenu(), model.getCurrentItem());
            view.setPrice("");
            model.restaurantProcessing.notifyAllObservers(null, Type.WAITER_MSG);

        }
    }

    class AddToCompositeListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            if((view.getSelectedFromMenu().equals(view.getSelectedToEdit()) && view.getCurrentPanel() == Panel.EDIT) || (view.getSelectedFromMenu().equals(model.getCurrentItem()) && view.getCurrentPanel() == Panel.CREATE)) {

                view.showMessage("Item is duplicate");
                return;
            }

            try {
                model.editComposition(view.getSelectedFromMenu(), Type.ADD);
            }catch (IOException exc){
                view.showMessage(exc.getMessage());
                return;
            }

            if(view.getCurrentPanel() == Panel.EDIT)
                view.refreshMenuEdit(null, null, model.getContentComposite(model.getCurrentItem()));

            view.showMessage("Successfully added item " + view.getSelectedFromMenu() + " to composition");
            model.setFinishedAdding(true);
            model.restaurantProcessing.notifyAllObservers("Composition of menu item '" + model.getCurrentItem() + "' has been changed", Type.CHEF_MSG);
        }
    }

    class DeleteItemListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            model.setCurrentItem(view.getSelectedFromMenu());
            model.deleteItem();
            view.showMessage("Successfully deleted item '"  + model.getCurrentItem() + "' from menu");

            view.refreshDeletePanel(model.getContentMenu());
            model.restaurantProcessing.notifyAllObservers("Item '" + model.getCurrentItem() + "' was deleted from menu", Type.CHEF_MSG);
        }
    }

    class NameOptionListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            if(view.isNameOptionSelected()) {
                view.setNameVisible(true);
            }else {

                view.setNameVisible(false);
                view.setName("");
            }

        }
    }

    class PriceOptionListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            if(view.isPriceOptionSelected())
                view.setPriceVisible(true);
            else{

                view.setPriceVisible(false);
                view.setPrice("");
            }
        }
    }

    class CompositeOptionListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            if (view.isCompositionOptionSelected() && model.isComposite(view.getSelectedToEdit())){

                view.setEditCompositeVisible(true);
                model.setCurrentItem(view.getSelectedToEdit());
                view.refreshMenuEdit(null, model.getCurrentItem(), model.getContentComposite(model.getCurrentItem()));

            }else{

                view.setEditCompositeVisible(false);
                view.setSelectedCompositeOption(false);
            }
        }
    }

    class FinishEditListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            model.setCurrentItem(view.getSelectedToEdit());

            if(view.isNameVisible()) {

                if(view.getName().equals("")){

                    view.showMessage("Name field is empty");
                    return;
                }

                try {
                    model.editName(view.getName());
                }catch (IOException exc){
                    view.showMessage(exc.getMessage());
                    return;
                }

                view.setName("");
            }

            if(view.isPriceVisible()) {

                if(view.isPriceVisible()){

                    if(view.getPrice().equals("")){

                        view.showMessage("Price field is empty");
                        return;
                    }
                }
                try {
                    model.editPrice(view.getPrice());
                }catch (IOException exc){

                    view.showMessage(exc.getMessage());
                    view.setPrice("");
                    return;
                }

                view.setPrice("");
            }

            view.refreshMenuEdit(model.getContentMenu(), null, model.getContentComposite(model.getCurrentItem()));
            model.restaurantProcessing.notifyAllObservers(null, Type.WAITER_MSG);
        }
    }

    class SelectToEditListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            model.setCurrentItem(view.getSelectedToEdit());
            view.refreshMenuEdit(null, null, model.getContentComposite(model.getCurrentItem()));
            view.addSelectFromMenuToEditListener(new SelectToEditListener());

            if(!model.isComposite(model.getCurrentItem())) {

                view.setSelectedCompositeOption(false);
                view.setEditCompositeVisible(false);
            }
        }
    }

    class DeleteFromCompositeListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            model.setCurrentItem(view.getSelectedToEdit());

            try {

                model.editComposition(view.getSelectedComponent(), Type.REMOVE);

            }catch (IOException exc){

                view.showMessage(exc.getMessage());
                return;
            }

            view.refreshMenuEdit(null, null, model.getContentComposite(model.getCurrentItem()));
            model.restaurantProcessing.notifyAllObservers("Composition of menu item '" + model.getCurrentItem() + "' has been changed", Type.CHEF_MSG);
        }
    }
}

package PresentationLayer.Admministrator;

import DataLayer.DataPack.Validation;
import BussinesLayer.EnumPack.Type;
import BussinesLayer.Restaurant.*;

import java.io.IOException;
import java.util.Arrays;

public class AdministratorModel {

    public Restaurant restaurantProcessing;
    private String currentItem;
    private Type currentTypeSelected = Type.BASE;
    private boolean finishedAdding = true;

    public void setRestaurantProcessing(Restaurant restaurantProcessing) {
        this.restaurantProcessing = restaurantProcessing;
    }

    public AdministratorModel(){

        restaurantProcessing = new Restaurant();

    }

    public Restaurant getRestaurantProcessing() {
        return restaurantProcessing;
    }

    public void deleteItem(){

        restaurantProcessing.deleteMenuItem(currentItem);
        restaurantProcessing.saveData();
    }

    public void setCurrentTypeSelected(Type type){

        currentTypeSelected = type;
    }

    public void addItem(String price) throws IOException{

        try {
            restaurantProcessing.createNewMenuItem(currentItem, Validation.validateFloat(price), currentTypeSelected);
            restaurantProcessing.saveData();
        }catch (IOException e){

            throw e;
        }
    }

    public void addBaseToComposite(String toBeAdded) throws IOException{

        restaurantProcessing.editMenuItem(currentItem, null, 0.0f, toBeAdded, Type.ADD);
        restaurantProcessing.saveData();
    }

    public void editName(String newName) throws IOException{

            restaurantProcessing.editMenuItem(currentItem, newName, 0.0f, null, null);
            restaurantProcessing.saveData();
            currentItem = newName;
    }

    public void editPrice(String price) throws IOException{

            restaurantProcessing.editMenuItem(currentItem, null, Validation.validateFloat(price), null, null);
            restaurantProcessing.saveData();
    }

    public void editComposition(String toBeEdited, Type operation) throws IOException{

        restaurantProcessing.editMenuItem(currentItem, null, 0.0f, toBeEdited, operation);
        restaurantProcessing.saveData();
    }

    public Type getCurrentTypeSelected() {
        return currentTypeSelected;
    }

    public String getCurrentItem() {
        return currentItem;
    }

    public void setCurrentItem(String currentItem) {
        this.currentItem = currentItem;
    }

    public boolean isFinishedAdding() {
        return finishedAdding;
    }

    public void setFinishedAdding(boolean finishedAdding) {
        this.finishedAdding = finishedAdding;
    }

    public String[] getContentMenu(){

        return restaurantProcessing.getMenuNames();
    }

    public String[] getContentComposite(String item){

        if(restaurantProcessing.getMenu().get(item) instanceof CompositeProduct)
            return Arrays.copyOf(((CompositeProduct)restaurantProcessing.getMenu().get(item)).getItems().keySet().toArray(), ((CompositeProduct) restaurantProcessing.getMenu().get(item)).getItems().keySet().size(), String[].class);

        return null;
    }

    public String[][] getMenuDetails(){

        return restaurantProcessing.getMenuDetails();
    }

    public boolean isComposite(String item){

        return restaurantProcessing.getMenu().get(item) instanceof CompositeProduct;
    }


}

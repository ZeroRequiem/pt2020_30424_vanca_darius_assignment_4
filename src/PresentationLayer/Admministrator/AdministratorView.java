package PresentationLayer.Admministrator;

import BussinesLayer.EnumPack.Panel;
import PresentationLayer.JComponents.*;

import javax.swing.JPanel;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;

public class AdministratorView extends JFrame{

    private Panel currentPanel;

    private JTextField name = new JTextField();
    private JTextField price = new JTextField();

    private JLabel nameLabel = new JLabel("Name");
    private JLabel priceLabel = new JLabel("Price");
    private JLabel menuLabel = new JLabel("Menu");
    private JLabel selectLabel = new JLabel("Select:");

    private String[] choices = {"Base", "Composite"};

    private JPanel mainPanel = new JPanel();
    private JPanel createPanel = new CustomPanel();
    private JPanel createPanelMain = new JPanel();
    private JPanel createBase = new CustomPanel(400, 100);
    private JPanel createComposite = new CustomPanel(400, 70);
    private JPanel editComposite = new CustomPanel(400, 100);

    private JRadioButton nameOption = new JRadioButton("Name");
    private JRadioButton priceOption = new JRadioButton("Price");
    private JRadioButton compositionOption = new JRadioButton("Composition");

    private JPanel selectFromMenu = new CustomPanel();
    private JPanel editMainPanel = new JPanel();
    private JPanel selectEditOptions = new CustomPanel(400, 40);
    private JPanel editPanel = new CustomPanel(400, 100);

    private JPanel deleteMainPanel = new JPanel();
    private JPanel deletePanel = new JPanel();

    private JComboBox options = new CustomComboBox(choices);
    private JComboBox menu;
    private JComboBox compositionList = new CustomComboBox(new String[]{"Empty..."});
    private JComboBox selectFromMenuToEdit = new CustomComboBox(new String[]{"Empty..."});

    private JButton doIt = new JButton("GO");

    private JPanel selectPanel = new JPanel();
    private JButton createButton = new CustomButton("Create");
    private JButton editButton = new CustomButton("Edit");
    private JButton deleteButton = new CustomButton("Delete");
    private JButton displayButton = new CustomButton("Display");

    private JTable menuTable = new JTable(new String[][]{{"Empty", "Empty", "Empty"}, {"Empty"}}, new String[]{"Name", "Price", "Composition"});
    private JPanel displayItemsPanel = new JPanel();
    private JPanel displayPanel = new CustomPanel(400, 400);
    private JScrollPane tableScroll;
    
    private JButton removeFromCompositeButton = new CustomButton("Remove");
    private JButton addBaseToCompositeButton = new CustomButton("Add");
    private JButton addItemButton = new CustomButton("Add Item");
    private JButton finishEditButton = new CustomButton("Finish");
    private JButton finishDeleteButton = new CustomButton("Finish");

    private void generateSelectPanel(){

        selectPanel.add(createButton);
        selectPanel.add(Box.createRigidArea(new Dimension(10, 0)));
        selectPanel.add(editButton);
        selectPanel.add(Box.createRigidArea(new Dimension(10, 0)));
        selectPanel.add(deleteButton);
        selectPanel.add(Box.createRigidArea(new Dimension(10, 0)));
        selectPanel.add(displayButton);


        selectPanel.setPreferredSize(new Dimension(400, 60));
        selectPanel.setBackground(new Color(50,50,50));
        selectPanel.setBorder(new EmptyBorder(10,10,10,10));
    }

    private void setDisplayItemsPanel(){

        displayItemsPanel.setBounds(0, 0, 600, 350);
        displayItemsPanel.setBackground(Color.darkGray);
        displayItemsPanel.setBorder(new EmptyBorder(25, 10, 10, 10));
    }

    private void generateDisplayItemsPanel(int ratio){

        displayItemsPanel.removeAll();
        displayItemsPanel.revalidate();
        displayItemsPanel.repaint();

        displayItemsPanel.add(selectPanel);

        tableScroll = new JScrollPane(menuTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        menuTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        tableScroll.setPreferredSize(new Dimension(400, 360));
        displayPanel.setBorder(new EmptyBorder(0, 0, 0, 0));
        TableColumnModel tableColumnModel = menuTable.getColumnModel();
        tableColumnModel.getColumn(2).setPreferredWidth((int)(0.6 * 400));
        tableColumnModel.getColumn(1).setPreferredWidth((int)(0.15 * 400));
        tableColumnModel.getColumn(0).setPreferredWidth((int)(0.25 * 400));
        tableColumnModel.setColumnSelectionAllowed(false);
        tableScroll.getViewport().setBackground(new Color(50, 50, 50));

        displayPanel.removeAll();
        displayPanel.revalidate();
        displayPanel.repaint();

        displayPanel.add(tableScroll);

        displayItemsPanel.add(displayPanel);
    }

    private void generateCreatePanel(){

        createPanelMain.removeAll();
        createPanelMain.revalidate();
        createPanelMain.repaint();

        createPanelMain.add(selectPanel);

        createPanel.add(options);
        createPanel.add(addItemButton);

        createBase.removeAll();
        createBase.revalidate();
        createBase.repaint();

        createBase.add(nameLabel);
        createBase.add(name);
        createBase.add(priceLabel);
        createBase.add(price);

        createComposite.removeAll();
        createComposite.revalidate();
        createComposite.repaint();

        createComposite.add(menuLabel);
        createComposite.add(Box.createRigidArea(new Dimension(10, 0)));
        createComposite.add(menu);
        createComposite.add(Box.createRigidArea(new Dimension(10, 0)));
        createComposite.add(addBaseToCompositeButton);

        createPanelMain.add(createPanel);
        createPanelMain.add(createBase);
        createPanelMain.add(createComposite);

        createComposite.setVisible(false);
        setNameVisible(true);
        setPriceVisible(true);
    }

    private void setCreatePanel(){

        createPanelMain.setBounds(0, 0, 600, 350);
        createPanelMain.setBackground(Color.darkGray);
        createPanelMain.setBorder(new EmptyBorder(25, 10, 10, 10));

        name.setPreferredSize(new Dimension(255, 30));
        price.setPreferredSize(new Dimension(255, 30));

        name.setBorder(BorderFactory.createCompoundBorder(name.getBorder(), BorderFactory.createEmptyBorder(5, 5, 5, 5)));
        price.setBorder(BorderFactory.createCompoundBorder(price.getBorder(), BorderFactory.createEmptyBorder(5, 5, 5, 5)));

        name.setBackground(Color.darkGray);
        name.setForeground(Color.lightGray);

        price.setBackground(Color.darkGray);
        price.setForeground(Color.lightGray);

        name.setFont(new Font("Consolas", Font.BOLD, 16));
        name.setCaretColor(Color.lightGray);

        price.setFont(new Font("Consolas", Font.BOLD, 16));
        price.setCaretColor(Color.lightGray);

        createBase.setLayout(new GridLayout(2, 2, -250, 15));
        nameLabel.setForeground(Color.lightGray);
        priceLabel.setForeground(Color.lightGray);

        setMenu(new String[]{"Empty..."});

        menuLabel.setBackground(Color.darkGray);
        menuLabel.setForeground(Color.lightGray);

        createComposite.setBorder(new EmptyBorder(20, 15, 20, 20));

    }

    private void setEditPanel(){

        editMainPanel.setBounds(0, 0, 600, 350);
        editMainPanel.setBackground(Color.darkGray);
        editMainPanel.setBorder(new EmptyBorder(25, 10, 10, 10));

        nameOption.setBackground(new Color(50,50,50));
        priceOption.setBackground(new Color(50,50,50));
        compositionOption.setBackground(new Color(50,50,50));

        nameOption.setForeground(Color.lightGray);
        priceOption.setForeground(Color.lightGray);
        compositionOption.setForeground(Color.lightGray);

        selectEditOptions.setLayout(new GridLayout(1, 3, 0, 10));
        selectEditOptions.add(nameOption);
        selectEditOptions.add(priceOption);
        selectEditOptions.add(compositionOption);

        selectEditOptions.setBorder(new EmptyBorder(5, 30, 10, 5));

        editPanel.setLayout(new GridLayout(2, 2, -250, 15));

        editComposite.setLayout(new GridLayout(2, 2, 5, 15));

        selectLabel.setForeground(Color.lightGray);
        selectLabel.setBackground(Color.darkGray);

    }

    private void generateEditPanel(){

        editMainPanel.removeAll();
        editMainPanel.revalidate();
        editMainPanel.repaint();

        editMainPanel.add(selectPanel);

        selectFromMenu.removeAll();
        selectFromMenu.revalidate();
        selectFromMenu.repaint();

        selectFromMenu.add(selectLabel);
        selectFromMenu.add(selectFromMenuToEdit);
        selectFromMenu.add(finishEditButton);

        editMainPanel.add(selectFromMenu);
        editMainPanel.add(selectEditOptions);

        editPanel.removeAll();
        editPanel.revalidate();
        editPanel.repaint();

        editPanel.add(nameLabel);
        editPanel.add(name);
        editPanel.add(priceLabel);
        editPanel.add(price);

        editComposite.removeAll();
        editComposite.revalidate();
        editComposite.repaint();

        editComposite.add(addBaseToCompositeButton);
        editComposite.add(menu);
        editComposite.add(removeFromCompositeButton);
        editComposite.add(compositionList);

        editMainPanel.add(editPanel);

        editMainPanel.add(editComposite);

    }

    private void setDeletePanel(){

        deleteMainPanel.setBounds(0, 0, 600, 350);
        deleteMainPanel.setBackground(Color.darkGray);
        deleteMainPanel.setBackground(Color.darkGray);
        deleteMainPanel.setBorder(new EmptyBorder(25, 10, 10, 10));

        deletePanel.setPreferredSize(new Dimension(400, 60));
        deletePanel.setBackground(new Color(50,50,50));
        deletePanel.setBorder(new EmptyBorder(10,-40,10,10));

    }

    private void generateDeletePanel(){

        deleteMainPanel.removeAll();
        deleteMainPanel.revalidate();
        deleteMainPanel.repaint();

        deleteMainPanel.add(selectPanel);

        deletePanel.removeAll();
        deletePanel.revalidate();
        deletePanel.repaint();

        deletePanel.add(selectLabel);
        deletePanel.add(menu);
        deletePanel.add(finishDeleteButton);

        deleteMainPanel.add(deletePanel);
    }

    private void setEverything(){

        currentPanel = Panel.CREATE;
        generateSelectPanel();
        setCreatePanel();
        setEditPanel();
        setDeletePanel();
        setDisplayItemsPanel();
    }

    public AdministratorView(){

        this.setPreferredSize(new Dimension(600,400));
        this.setBackground(Color.DARK_GRAY);
        this.setResizable(false);
        this.setTitle("Administrator Window");
        mainPanel.setBackground(Color.DARK_GRAY);
        mainPanel.setForeground(Color.DARK_GRAY);
        mainPanel.setPreferredSize(new Dimension(600,350));


        setEverything();
        generateCreatePanel();

        mainPanel.setBorder(new EmptyBorder(-10, 10, 10, 10));
        mainPanel.setLayout(new GridLayout(0, 1, 0, 0));
        mainPanel.add(createPanelMain);

        this.setContentPane(mainPanel);
        this.setVisible(true);
        this.pack();

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

    public void switchToCreate(){

        currentPanel = Panel.CREATE;
        this.setResizable(true);
        this.setPreferredSize(new Dimension(600,400));
        this.setResizable(false);
        this.pack();
        mainPanel.removeAll();
        mainPanel.revalidate();
        mainPanel.repaint();
        generateCreatePanel();
        mainPanel.add(createPanelMain);

    }

    public void switchToEdit(){

        currentPanel = Panel.EDIT;
        this.setResizable(true);
        this.setPreferredSize(new Dimension(600,500));
        this.pack();
        this.setResizable(false);
        mainPanel.removeAll();
        mainPanel.revalidate();
        mainPanel.repaint();
        generateEditPanel();
        mainPanel.add(editMainPanel);

        setNameVisible(false);
        setPriceVisible(false);

        editComposite.setVisible(false);
    }

    public void switchToDisplay(int ratio){

        currentPanel = Panel.VIEW;
        this.setResizable(true);
        this.setPreferredSize(new Dimension(600,500));
        this.pack();
        this.setResizable(false);

        mainPanel.removeAll();
        mainPanel.revalidate();
        mainPanel.repaint();
        generateDisplayItemsPanel(ratio);
        mainPanel.add(displayItemsPanel);
    }

    public void switchToDelete(){

        currentPanel = Panel.DELETE;
        this.setResizable(true);
        this.setPreferredSize(new Dimension(600,250));
        this.setResizable(false);
        this.pack();
        mainPanel.removeAll();
        mainPanel.revalidate();
        mainPanel.repaint();
        generateDeletePanel();
        mainPanel.add(deleteMainPanel);
    }

    public void resetPanels(){

        if(currentPanel == Panel.DELETE)
            resetDeletePanel();

        if(currentPanel == Panel.CREATE)
            resetCreatePanel();

        if(currentPanel == Panel.EDIT)
            resetEditPanel();
    }

    private void resetCreatePanel(){

        name.setText("");
        price.setText("");

        options.setSelectedIndex(0);

        if(menu.getItemCount() >= 1)
            menu.setSelectedIndex(0);
    }

    private void resetEditPanel(){

        nameOption.setSelected(false);
        priceOption.setSelected(false);
        compositionOption.setSelected(false);

        if(selectFromMenuToEdit.getItemCount() >= 1)
            selectFromMenuToEdit.setSelectedIndex(0);

        if(menu.getItemCount() >= 1)
            menu.setSelectedIndex(0);

        compositionList.setSelectedIndex(0);

        name.setText("");
        price.setText("");
    }

    private void resetDeletePanel(){

        if(menu.getItemCount() >= 1)
            menu.setSelectedIndex(0);
    }

    public void setName(String newString){

        name.setText(newString);
    }

    public void setPrice(String newString){

        price.setText(newString);
    }

    public void setSelectFromMenuToEdit(String[] content){

        selectFromMenuToEdit = new CustomComboBox(content);
    }

    public void setMenu(String[] menu){

        if(menu.length > 0)
            this.menu = new CustomComboBox(menu);
        else
            this.menu = new CustomComboBox(new String[]{"Empty..."});
    }

    public void setCompositeList(String[] content){

        compositionList = new CustomComboBox(content);
    }

    public String getSelectedComponent(){

        return (String) compositionList.getSelectedItem();
    }

    public String getSelectedFromMenu(){

        return (String) menu.getSelectedItem();
    }

    public String getOption() {

        return (String) options.getSelectedItem();
    }

    public String getSelectedToEdit(){

        return (String) selectFromMenuToEdit.getSelectedItem();
    }

    public void addDiplayButtonListener(ActionListener cbl){

        displayButton.addActionListener(cbl);
    }

    public void addDeleteButtonListener(ActionListener cbl){

        deleteButton.addActionListener(cbl);
    }

    public void addEditButtonListener(ActionListener cbl){

        editButton.addActionListener(cbl);
    }

    public void addCreateButtonListener(ActionListener cbl){

        createButton.addActionListener(cbl);
    }

    public void addFinishDeleteListener(ActionListener cbl){

        finishDeleteButton.addActionListener(cbl);
    }

    public void addCompostionOptionListener(ActionListener cbl){

        compositionOption.addActionListener(cbl);
    }

    public void addPriceOptionListener(ActionListener cbl){

        priceOption.addActionListener(cbl);
    }

    public void addNameOptionListener(ActionListener cbl){

        nameOption.addActionListener(cbl);
    }

    public void addDeleteFromCompositeListener(ActionListener cbl){

        removeFromCompositeButton.addActionListener(cbl);
    }

    public void addFinishEditButtonListener(ActionListener cbl){

        finishEditButton.addActionListener(cbl);
    }

    public void addSelectFromMenuToEditListener(ActionListener cbl){

        selectFromMenuToEdit.addActionListener(cbl);
    }

    public void addBaseToCompositeButtonListener(ActionListener cbl){

        addBaseToCompositeButton.addActionListener(cbl);
    }

    public void addCreateItemButtonListener(ActionListener cbl){

        addItemButton.addActionListener(cbl);
    }

    public void addOptionsListener(ActionListener cbl){

        options.addActionListener(cbl);
    }

    public void setNameVisible(boolean val){

        this.nameLabel.setVisible(val);
        this.name.setVisible(val);
    }

    public void setPriceVisible(boolean val){

        this.priceLabel.setVisible(val);
        this.price.setVisible(val);
    }

    public void setEditCompositeVisible(boolean val){

        this.editComposite.setVisible(val);
    }

    public void setCreateCompositeVisible(boolean val){

        this.createComposite.setVisible(val);
    }

    public boolean isPriceOptionSelected(){

        return priceOption.isSelected();
    }

    public boolean isNameOptionSelected(){

        return nameOption.isSelected();
    }

    public boolean isCompositionOptionSelected(){

        return compositionOption.isSelected();
    }

    public void setCurrentPanel(Panel panel){

        currentPanel = panel;
    }

    public String getPrice() {
        return price.getText();
    }

    public Panel getCurrentPanel(){

        return currentPanel;
    }

    public void showMessage(String msg){

        JOptionPane.showMessageDialog(this, msg);
    }

    public void refreshMenuEdit(String[] contentMenu, String exclude, String[] contentComposite){

        if(exclude != null && contentMenu != null) {

            ArrayList<String> aux = new ArrayList<String>(Arrays.asList(contentMenu));
            aux.remove(exclude);
            contentMenu = Arrays.copyOf(aux.toArray(), aux.size(), String[].class);
        }

        if(contentMenu != null) {

            if (contentMenu.length == 0)
                selectFromMenuToEdit = new CustomComboBox(new String[]{"Empty..."});
            else
                selectFromMenuToEdit = new CustomComboBox(contentMenu);

            setMenu(contentMenu);
        }

        if(contentComposite != null)
            compositionList = new CustomComboBox(contentComposite);
        else
            compositionList = new CustomComboBox(new String[]{"Empty..."});

        editComposite.removeAll();
        editComposite.revalidate();
        editComposite.repaint();

        editComposite.add(addBaseToCompositeButton);
        editComposite.add(menu);
        editComposite.add(removeFromCompositeButton);
        editComposite.add(compositionList);

        selectFromMenu.removeAll();
        selectFromMenu.revalidate();
        selectFromMenu.repaint();

        selectFromMenu.add(selectLabel);
        selectFromMenu.add(selectFromMenuToEdit);
        selectFromMenu.add(finishEditButton);
    }

    public void refreshMenuCreate(String[] contentMenu, String exclude){

        if(exclude != null) {

            ArrayList<String> aux = new ArrayList<String>(Arrays.asList(contentMenu));
            aux.remove(exclude);
            contentMenu = Arrays.copyOf(aux.toArray(), aux.size(), String[].class);
        }

        menu = new CustomComboBox(contentMenu);

        createComposite.removeAll();
        createComposite.revalidate();
        createComposite.repaint();

        createComposite.add(menuLabel);
        createComposite.add(Box.createRigidArea(new Dimension(10, 0)));
        createComposite.add(menu);
        createComposite.add(Box.createRigidArea(new Dimension(10, 0)));
        createComposite.add(addBaseToCompositeButton);

    }

    public void refreshDeletePanel(String[] content){

        if(content != null){

            if(content.length != 0)
                menu = new CustomComboBox(content);
            else
                menu = new CustomComboBox(new String[]{"Empty..."});
        }

        deletePanel.removeAll();
        deletePanel.revalidate();
        deletePanel.repaint();

        deletePanel.add(selectLabel);
        deletePanel.add(menu);
        deletePanel.add(finishDeleteButton);
    }

    public void refreshDisplayPanel(String[][] content){

        if(content != null){

            menuTable = new JTable(content, new String[]{"Name", "Price", "Composition"});
            menuTable.setGridColor(Color.lightGray);
            menuTable.setBounds(0, 0, 300, 100);
        }
    }

    public void setIndexOption(){

        options.setSelectedIndex(1);
    }

    public String getName(){

        return name.getText();
    }

    public boolean isNameVisible(){

        return name.isVisible();
    }

    public boolean isPriceVisible(){

        return price.isVisible();
    }

    public void setSelectedCompositeOption(boolean val){
        compositionOption.setSelected(false);
    }
}

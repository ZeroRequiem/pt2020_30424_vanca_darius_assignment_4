package Main;

import BussinesLayer.EnumPack.Type;
import PresentationLayer.Admministrator.AdministratorController;
import PresentationLayer.Admministrator.AdministratorModel;
import PresentationLayer.Admministrator.AdministratorView;
import PresentationLayer.Chef.ChefController;
import PresentationLayer.Chef.ChefModel;
import PresentationLayer.Chef.ChefView;
import DataLayer.DataPack.RestaurantSerialization;
import BussinesLayer.Restaurant.*;
import PresentationLayer.Waiter.WaiterController;
import PresentationLayer.Waiter.WaiterModel;
import PresentationLayer.Waiter.WaiterView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Start {

    public static void main(String[] args) {

        String location = null;

        if(args.length != 1){

            System.out.println("Incorrect number of parameters");
            System.exit(-1);

        }else
            location = args[0];

        Restaurant restaurant = RestaurantSerialization.obtainObject(location);

        AdministratorModel administratorModel = new AdministratorModel();
        administratorModel.setRestaurantProcessing(restaurant);
        new AdministratorController(administratorModel, new AdministratorView());

        ChefModel chefModel = new ChefModel();
        chefModel.setRestaurantProcessing(restaurant);
        new ChefController(chefModel, new ChefView());

        WaiterModel waiterModel = new WaiterModel();
        waiterModel.setRestaurantProcessing(restaurant);

        new WaiterController(waiterModel, new WaiterView());

    }
}

package BussinesLayer.ObserverPack;

import BussinesLayer.EnumPack.Type;

import java.io.Serializable;

public abstract class MyObserver implements Serializable {
    public abstract void update(String msg, Type type);
}

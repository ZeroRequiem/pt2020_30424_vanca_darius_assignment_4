package BussinesLayer.Restaurant;

import java.util.HashMap;

public class Helper {//search for composite items in the menu who have the item in them and replace them with the recently updated one,
                    //we do this because the key is dependent on the name
    public static void updateComposites(HashMap<String, MenuItem> menu, String oldKey, MenuItem item){

        if(oldKey != null) {

            for (MenuItem current : menu.values()) {

                if (current instanceof CompositeProduct) {

                    if (((CompositeProduct) current).getItems().containsKey(oldKey)) {

                        ((CompositeProduct) current).getItems().remove(oldKey);
                        ((CompositeProduct) current).getItems().put(item.getName(), item);
                    }
                }
            }
        }
    }
}

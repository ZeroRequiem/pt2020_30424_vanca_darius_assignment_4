package BussinesLayer.Restaurant;

import DataLayer.DataPack.MyFileWriter;
import BussinesLayer.EnumPack.Type;
import BussinesLayer.ObserverPack.*;

import java.io.IOException;
import java.util.*;
import java.util.List;

public class Restaurant implements IRestaurantProcessing, java.io.Serializable{

    /**
     * The map of orders, a order can have multiple items
     * The menu of items
     * Observers
     */
    private HashMap<Order, List<MenuItem>> map;
    private HashMap<String, MenuItem> menu;
    private List<MyObserver> observers;

    public Restaurant(){

        map = new HashMap<Order, List<MenuItem>>();
        menu = new HashMap<String, MenuItem>();
        observers = new ArrayList<MyObserver>();

        assert isAllGood();
    }

    /**invariant, holds true from till the death of the object
     * @return true if all fields are not null, false otherwise
     */
    public boolean isAllGood(){
        return map != null && menu != null && observers != null;
    }
    public List<MyObserver> getObservers(){return observers;}
    /**Method used to attach observer
     * @param observer the observer to be attached*/
    public void attachObserver(MyObserver observer){observers.add(observer);}

    /**Method used to notify observers
     * @param msg The message (if case)
     * @param type for whom (waiter or chef)
     * */
    public void notifyAllObservers(String msg, Type type){

        assert isAllGood();
        for(MyObserver current : observers)
            current.update(msg, type);
    }

    public void clearObservers(){observers.clear();}

    public void setObservers(List<MyObserver> observers) {this.observers = observers;}
    /**@return the map of orders
     */
    public HashMap<Order, List<MenuItem>> getMap() { return map; }

    public void setMap(HashMap<Order, List<MenuItem>> map) { this.map = map; }

    /**@return the menu of the restaurant
     */
    public HashMap<String, MenuItem> getMenu() { return menu; }

    public String[] getMenuNames(){
        assert isAllGood();
        return Arrays.copyOf(menu.keySet().toArray(), menu.keySet().size(), String[].class);
    }

    public void setMenu(HashMap<String, MenuItem> menu) {this.menu = menu;}

    /**Method used to add items to a composite product
     * @param compositeProduct the composite product
     * @param toBeAdded the item to be added
     */
    public void addItems(String compositeProduct, String toBeAdded){

        try{
            editMenuItem(compositeProduct,null, 0, toBeAdded, Type.ADD);
        }catch (IOException e){}
    }

    @Override
    public void createNewMenuItem(String name, float price, Type type) throws IOException{

        if(menu.containsKey(name))//precondition
            throw new IOException("Item already found in menu");

        assert isAllGood();

        if(price < 0)//precondition
            throw new IOException("Price cannot be negative");

        int oldSize = menu.keySet().size();

        if(type == Type.BASE){

            if(price == 0)
                throw new IOException("Price cannot be 0 for a base product");

            menu.put(name, new BaseProduct(name, price));//add a new base item

        }else if(type == Type.COMPOSITE){
            menu.put(name, new CompositeProduct(name));//add a new composite product
        }else
            throw new IOException("Invalid operation type");

        assert (oldSize + 1 == menu.keySet().size()) && (menu.keySet().size() >= 1);
    }

    @Override
    public void deleteMenuItem(String item) {

        int oldSize = menu.keySet().size();
        assert isAllGood();
        MenuItem toBeDeleted = menu.get(item);

        assert toBeDeleted != null: "Couldn't find item to edit";//check to make sure you found the item

        menu.remove(item, toBeDeleted);//remove it from menu
        MenuItem current = null;
        List<String> toDelete = new ArrayList<String>();//list of composite elements who share the item that was just deleted

        for(int i = 0; i < menu.values().size(); i++){

            current = Arrays.copyOf(menu.values().toArray(), menu.values().size(), MenuItem[].class)[i];

            if(current instanceof CompositeProduct)
                if(((CompositeProduct) current).getItems().containsKey(item)){
                    toDelete.add(current.getName());
            }
        }

        assert (menu.keySet().size() == oldSize - 1) && (menu.keySet().size() >= 0);

        for(String currentItem : toDelete)//remove them all
            deleteMenuItem(currentItem);
    }

    @Override
    public void editMenuItem(String item, String newName, float newPrice, String additionalItem, Type operation) throws IOException{

        MenuItem toBeAdded = null;
        String oldKey = null;
        assert isAllGood();
        if(additionalItem != null)//the operations vary, if additionalItem = null do nothing with it, else search it in the menu
            toBeAdded = menu.get(additionalItem);

        MenuItem mainItem = menu.get(item);//get item from the menu

        assert mainItem != null: "Couldn't find main item to edit";

        if(mainItem instanceof BaseProduct && toBeAdded != null)//if we have a base item as a main, and the additional item is not null => invalid operation
            throw new IOException("Product cannot be added for a base product");

        if(newName != null){//again, it varies, for newName == null it will ignore it
            menu.remove(item, mainItem);//since we have the name as key, we have to remove the old item
            menu.put(newName, mainItem);//and replace it with a new key (the new name)
        }

        if (mainItem instanceof BaseProduct) {//perform edit on base product
            if(newName != null) oldKey = mainItem.getName();
            mainItem.updateFields(newName, newPrice, Type.BASE);
            Helper.updateComposites(menu, oldKey, mainItem);
        }

        if (mainItem instanceof CompositeProduct)//perform edit on composite item
            ((CompositeProduct) mainItem).updateProduct(newName, newPrice, toBeAdded, operation);

        notifyAllObservers(null, Type.WAITER_MSG);//notify the observers

    }

    /**Method which will generate an order id, based on the last order in the map
     * @return a unique order ID
     */
    private int generateOrderId(){

        assert isAllGood();

        if(map.keySet().size() == 0)
            return 0;

        int orderID = ((Order)map.keySet().toArray()[map.keySet().size() - 1]).getOrderID() + 1;
        assert !map.containsKey(new Order(orderID, new Date())): "Map already contains orderId:" + orderID;

        return orderID;
    }

    @Override
    public void createNewOrder(List<String> items) {

        Order newOrder = new Order(generateOrderId(), new Date());//generate a new order
        assert isAllGood();
        int oldMapSize = map.keySet().size();//save the old size of map

        for (String current : items){//add all the items in the list
            assert menu.containsKey(current);
            newOrder.addMenuItem(new MenuItem(current, menu.get(current).getPrice()));
        }
        map.put(newOrder, newOrder.getTable());//be sure to put in the map

        assert map.keySet().size() == oldMapSize + 1;//check
        assert newOrder.getTable().size() == items.size();
    }

    /**Used to check consistency, checks the first column of each row
     * @param details the table of details
     * @param data the data to be compared to
     * @return true if everything is okey, false otherwise
     */
    private boolean isConsistent(String[][] details, String[] data){

        assert isAllGood();
        for(int row = 0; row < data.length; row++)
            if(!details[row][0].equals(data[row]))
                return false;

        return true;
    }

    @Override
    public String[][] getMenuDetails(){

        assert isAllGood();
        if(menu.keySet().size() == 0) return null;
        String[][] details = new String[menu.keySet().size()][3];//table of details
        int indexRow = 0;

        for(MenuItem current : menu.values()){

            details[indexRow][0] = current.getName();//first field, name of item
            details[indexRow][1] = String.valueOf(current.getPrice());//second, price

            if(current instanceof CompositeProduct)
                details[indexRow][2] = ((CompositeProduct) current).getCompositionToString();//third, the composition if it's a composite product
            else
                details[indexRow][2] = "Empty...";

            indexRow++;

        }

        assert isConsistent(details, Arrays.copyOf(menu.keySet().toArray(), menu.keySet().size(), String[].class));//check consistency
        return details;
    }

    @Override
    public float computePriceOrder(int orderId) {

        Order order = null;
        assert isAllGood();
        for(Order current : map.keySet())//search for order
            if(current.getOrderID() == orderId){
                order = current;
                break;
            }

        float price = 0;
        assert order != null: "Order couldn't be found";//validate

        for(MenuItem current : map.get(order))
            price += current.getPrice();//calculate the price as the sum of all the items in the order

        assert price > 0: "Price can't be negative";//validate the price, post-condition
        return price;
    }

    @Override
    public void generateBill(int orderId){

        Order order = null;

        for(Order current : map.keySet())//search for order
            if(current.getOrderID() == orderId){
                order = current;
                break;//if found break
            }

        assert order != null: "Order couldn't be found";//make sure you found it (precondition)

        MyFileWriter.generateBill(order, computePriceOrder(orderId), "Bill" + orderId + ".txt");//generate the bill

    }

    @Override
    public String[][] getOrdersDetails(){

        if(map.keySet().size() == 0) return null;
        String[][] details = new String[map.keySet().size()][4];//number of keys represents the number of entries in the table, we have 4 columns(fields)
        int indexRow = 0;
        assert isAllGood();

        for(Order current : map.keySet()){//iterate over to get values

            details[indexRow][0] = String.valueOf(current.getOrderID());//first field orderID
            details[indexRow][1] = current.getDate().toString();//second, the date
            details[indexRow][2] = String.valueOf(computePriceOrder(current.getOrderID()));//third, the total price of order

            if(map.get(current).size() != 0)//if list of items in order not empty
                details[indexRow][3] = current.getItemsListToString();//forth, composition list of order
            else
                details[indexRow][3] = "Empty...";//list is empty, no order

            indexRow++;
        }
        indexRow = 0;
        String[] orders = new String[map.keySet().size()];
        for(Order key : map.keySet())
            orders[indexRow++] = String.valueOf(key.getOrderID());
        assert isConsistent(details, orders);//check consistency

        return details;
    }

    public void saveData(){ MyFileWriter.saveToFile(this);}
}

package BussinesLayer.Restaurant;

import java.io.Serializable;
import java.util.*;
import java.util.List;

public class Order implements Serializable {

    private int orderID;
    private Date date;
    private List<MenuItem> table;

    public Order(int orderID, Date date){

        this.orderID = orderID;
        this.date = date;
        table = new ArrayList<MenuItem>();
    }

    public void addMenuItem(MenuItem item){

        table.add(item);
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<MenuItem> getTable() {
        return table;
    }

    public void setTable(List<MenuItem> table) {
        this.table = table;
    }

    @Override
    public boolean equals(Object obj) {

        assert obj instanceof Order : "Not instance of order";

        return this.orderID == ((Order)obj).getOrderID();
    }

    @Override
    public int hashCode() {

        return orderID;
    }

    public String getItemsListToString(){

        String buffer = "";
        int i = 0;

        for(MenuItem current : table){

            buffer += current.getName();

            if(i++ != table.size() - 1)
                buffer += ", ";
        }

        return buffer;
    }
}

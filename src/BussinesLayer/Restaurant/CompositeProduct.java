package BussinesLayer.Restaurant;

import BussinesLayer.EnumPack.Type;

import java.io.IOException;
import java.util.*;

public class CompositeProduct extends MenuItem{

    private HashMap<String, MenuItem> items;

    public CompositeProduct(String name){

        super(name, 0f);

        items = new HashMap<String, MenuItem>();
    }

    public void addItem(MenuItem item){

        items.put(item.getName(), item);
        super.setPrice(super.getPrice() + item.getPrice());
    }

    public void removeItem(MenuItem item){

        items.remove(item.getName(), item);
        super.setPrice(super.getPrice() - item.getPrice());
    }

    public HashMap<String, MenuItem> getItems() {
        return items;
    }

    public void updateProduct(String newName, float newPrice, MenuItem toBeAdded, Type operation) throws IOException {

        super.updateFields(newName, newPrice, Type.COMPOSITE);

        if(toBeAdded != null) {

            if (operation == Type.ADD) {

                if(items.containsValue(toBeAdded))
                    throw new IOException("Item already found in composition");
                else
                    addItem(toBeAdded);
            }

            if (operation == Type.REMOVE) {

                if (items.containsValue(toBeAdded)) {

                    if(items.size() == 1)
                        throw  new IOException("Removing this item will result in a composite product with no composition, use the 'Delete' tab if you wish to delete the item");

                    removeItem(toBeAdded);
                }else
                    throw new IOException("Item couldn't be found in composition list");
            }
        }

    }

    @Override
    public float computePrice() {

        float price = 0f;

        for(MenuItem current : items.values()){

            price += current.getPrice();
        }

        super.setPrice(price);

        return price;
    }

    public String getCompositionToString(){

        String buffer = "";
        int i = 0;

        for(MenuItem current : items.values()){

            buffer += current.getName();

            if(i++ != items.values().size() - 1)
                buffer += ", ";
        }

        return buffer;
    }

    public int getCompositionSize(){

        return items.values().size();
    }
}

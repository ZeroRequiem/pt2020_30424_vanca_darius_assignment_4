package BussinesLayer.Restaurant;

import BussinesLayer.EnumPack.Type;

import java.io.IOException;
import java.util.*;

public interface IRestaurantProcessing {

    /**
     * preconditions: type is base/composite, price bigger or equal to 0, it is a new item
     * post-conditions: size == oldSize + 1, size bigger or equal to 1
     * @param name name of menu item
     * @param price price of menu item
     * @param type type of item (base/composite)
     * @throws IOException exception in case of input problems, type is not BASE/COMPOSITE, price smaller than 0, item already found in menu
     */
    public void createNewMenuItem(String name, float price, Type type) throws IOException;

    /**
     * post-conditions: size == oldSize - (number of composite items who have that item in their composition) - 1
     * @param item the item to be deleted
     */
    public void deleteMenuItem(String item);

    /**
     * @param item item to be edited
     * @param newName new name, if any
     * @param newPrice new price
     * @param additionalItem item to be added/removed from composition (if item is composite), it will be null if we're dealing with a base product
     * @param operation type of operation, (add to composite/remove from composite)
     * @throws IOException  exception will be thrown if the main item is a base item and someone tries to perform a composite type of operation on it
     */
    public void editMenuItem(String item, String newName, float newPrice, String additionalItem, Type operation) throws IOException;

    /**
     * post-conditions: mapCount == oldMapCount + 1, newOrder's number of items == items.count()
     * @param items items which make part of the order
     */
    public void createNewOrder(List<String> items);

    /**
     * preconditions: order must be in the map
     * post-conditions: price must be bigger than 0
     * @param orderId id of the order, which acts as a key
     * @return returns the computed price of the order
     */
    public float computePriceOrder(int orderId);

    /**
     * preconditions: order must be in map
     * @param orderId the id of the order for which the bill will be generated
     */
    public void generateBill(int orderId);

    /**
     * post-conditions: there must be some consistency of
     * @return a table with details about all the orders in the form of a matrix of strings
     */
    public String[][] getOrdersDetails();

    /**
     * post-conditions: there must be some consistency
     * @return a table with details about all the orders in the form of a matrix of strings
     */
    public String[][] getMenuDetails();
}

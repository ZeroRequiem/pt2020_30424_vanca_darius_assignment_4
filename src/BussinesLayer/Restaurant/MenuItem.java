package BussinesLayer.Restaurant;

import BussinesLayer.EnumPack.Type;

import java.io.IOException;

public class MenuItem implements java.io.Serializable{

    private String name;
    private float price;

    public MenuItem(String name, float price){

        this.name = name;
        this.price = price;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float computePrice(){

        return price;
    };

    public void updateFields(String newName, float newPrice, Type type) throws IOException{

        if(newPrice < 0)
            throw new IOException("Price cannot be negative");

        if(newName != null){

            if(!newName.equals(name))
                name = newName;
        }

        if(newPrice >= 0 && type == Type.COMPOSITE){

            if(newPrice != price)
                price = newPrice;
        }
    }

    @Override
    public boolean equals(Object obj) {

        if(obj instanceof MenuItem) {

            return this.name.equals(((MenuItem) obj).getName());
        }

        return false;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
